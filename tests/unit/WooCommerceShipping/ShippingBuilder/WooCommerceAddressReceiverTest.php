<?php


use WPDesk\WooCommerceShipping\ShippingBuilder\WooCommerceAddressReceiver;

class WooCommerceAddressReceiverTest extends \PHPUnit\Framework\TestCase {
	public function test_can_get_valid_address() {
		$woocommerce_package = [
			'destination' => [
				'address' => 'address value',
				'address2' => 'address value 2',
				'city' => 'city value',
				'postcode' => '12-123',
				'country' => 'PL',
				'state' => 'PL-DS'
			]
		];

		$address_receiver = new WooCommerceAddressReceiver($woocommerce_package);
		$address = $address_receiver->get_address();

		$this->assertEquals($woocommerce_package['destination']['address'], $address->address_line1);
		$this->assertEquals($woocommerce_package['destination']['address2'], $address->address_line2);
		$this->assertEquals($woocommerce_package['destination']['city'], $address->city);
		$this->assertEquals($woocommerce_package['destination']['postcode'], $address->postal_code);
		$this->assertEquals($woocommerce_package['destination']['country'], $address->country_code);
		$this->assertEquals($woocommerce_package['destination']['state'], $address->state_code);
	}
}

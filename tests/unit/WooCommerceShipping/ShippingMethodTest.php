<?php

use Psr\Log\NullLogger;
use WPDesk\AbstractShipping\Settings\SettingsDefinition;
use WPDesk\AbstractShipping\ShippingService;
use WPDesk\WooCommerceShipping\ShippingMethod;

/**
 * Class ShippingMethodTest
 */
class ShippingMethodTest extends \PHPUnit\Framework\TestCase {

	protected function setUp() {
		\WP_Mock::setUp();
		$this->markTestSkipped('We add tests after stabilizing');

		$shippingService = $this->createMock( ShippingService::class );
		$shippingService
			->method( 'get_settings_definition' )
			->willReturn( $this->createMock( SettingsDefinition::class ) );

		ShippingMethod::set_shipping_service( $shippingService );
		ShippingMethod::set_logger( new NullLogger() );
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_can_run_calculate_shipping() {
		\WP_Mock::userFunction( 'get_option', array(
			'return' => function ( $name ) {
				return '';
			}
		) );

		$method = new ShippingMethod();
		$method->calculate_shipping( [] );
		$this->assertTrue(true, 'Should be here without exception thrown');
	}

	public function test_can_run_generate_settings_html() {
		$method = new ShippingMethod();
		$html = $method->generate_settings_html([], false);
		$this->assertEmpty($html, 'No fields so no rendered settings');
	}

	public function test_can_run_validate_services_field() {
		$method = new ShippingMethod();
		$method->validate_services_field('', []);
		$this->assertTrue(true, 'Should be here without exception thrown');
	}
}


/**
 * WC_Shipping_Method Stub class for WPDesk\WooCommerceShipping\ShippingMethod to extend
 */
class WC_Shipping_Method {
	/**
	 * no settings
	 *
	 * @var array
	 */
	public $settings = [];


	public function __construct( $instance ) {
	}

	/**
	 * no settings
	 */
	public function init_settings() {
	}

	/**
	 * return 'no' for all get_options attempts (no debug for example)
	 *
	 * @return string
	 */
	public function get_option() {
		return 'no';
	}

	/**
	 * no fields
	 *
	 * @return array
	 */
	public function get_form_fields() {
		return [];
	}
}

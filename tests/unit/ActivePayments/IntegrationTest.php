<?php

use WPDesk\WooCommerceShipping\ActivePayments\Integration;

class IntegrationTest extends \PHPUnit\Framework\TestCase {


	const CURRENT_SHIPPING_METHOD_ID                          = 'test_shipping';
	const ACTIVE_PAYMENTS_SHIPPING_METHOD_ID                  = 'test_shipping:0';
	const CURRENT_CART_SHIPPING_METHOD_ID_WITH_SERVICE        = 'test_shipping:0:EXPRESS';
	const CURRENT_CART_SHIPPING_METHOD_ID_TO_COLLECTION_POINT = 'test_shipping:0:EXPRESS:collection-point';

	const OTHER_ACTIVE_PAYMENTS_SHIPPING_METHOD_ID   = 'other_shipping:0';
	const OTHER_CART_SHIPPING_METHOD_ID_WITH_SERVICE = 'other_shipping:0:AIR';

	public function setUp() {
		\WP_Mock::setUp();
	}

	public function tearDown() {
		\WP_Mock::tearDown();
	}

	public function test_matched_method() {
		$integration = new Integration( self::CURRENT_SHIPPING_METHOD_ID );

		$this->assertEquals(
			self::ACTIVE_PAYMENTS_SHIPPING_METHOD_ID,
			$integration->get_shipping_method_for_active_payments_checkout( self::CURRENT_CART_SHIPPING_METHOD_ID_WITH_SERVICE ),
			'Shipping method for AP should be processed for matched method'
		);

		$this->assertEquals(
			self::ACTIVE_PAYMENTS_SHIPPING_METHOD_ID,
			$integration->get_shipping_method_for_active_payments_checkout( self::CURRENT_CART_SHIPPING_METHOD_ID_TO_COLLECTION_POINT ),
			'Shipping method for AP to collection point should be processed for matched method'
		);
	}

	public function test_not_matched_method() {
		$integration = new Integration( self::CURRENT_SHIPPING_METHOD_ID );

		$this->assertEquals(
			self::OTHER_CART_SHIPPING_METHOD_ID_WITH_SERVICE,
			$integration->get_shipping_method_for_active_payments_checkout( self::OTHER_CART_SHIPPING_METHOD_ID_WITH_SERVICE ),
			'Shipping method for AP should not be processed for not matched method'
		);

	}
}

[![pipeline status](https://gitlab.com/wpdesk/wp-woocommerce-shipping/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wp-woocommerce-shipping/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wp-woocommerce-shipping/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wp-woocommerce-shipping/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/v/stable)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/downloads)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/v/unstable)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 
[![License](https://poser.pugx.org/wpdesk/wp-woocommerce-shipping/license)](https://packagist.org/packages/wpdesk/wp-woocommerce-shipping) 

WooCommerce Shipping
====================

